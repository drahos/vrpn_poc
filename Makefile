all: monitor monitor_c

monitor: vrpn_tracker.o monitor.o vrpn/build
	$(CXX) $(CXXFLAGS) -o monitor vrpn_tracker.o monitor.o -lvrpn -lpthread -lquat

monitor_c: vrpn_tracker.o monitor_c.o vrpn/build
	$(CXX) $(CFLAGS) -o monitor_c vrpn_tracker.o monitor_c.o -lvrpn -lpthread -lquat


vrpn/build:
	mkdir vrpn/build
	cd vrpn/build && cmake .. && make

clean:
	rm -f monitor monitor_c *.o

export CXXFLAGS=-std=gnu++11 -Lvrpn/build -Lvrpn/build/quat -g
export CPPFLAGS=-I./vrpn/
export CFLAGS=-std=gnu11 -Lvrpn/build -Lvrpn/build/quat -g
