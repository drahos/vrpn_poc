#include <iostream>
#include <cstdio>

#include <unistd.h>
#include <functional>

#include "vrpn_tracker.hpp"

using namespace std;
using namespace microcart;

static int count_messages = 0;

static void cb(const TrackerData &td)
{
	count_messages++;
}

int main(void)
{
	Tracker t(string("UAV@192.168.0.120:3883"));
	cout << "Created tracker!" << endl;
	t.addCallback(cb);
	for (int i = 0; i < 15; i++) {
		auto td = t.getData();
		printf("(%d) Pos (xyz): (%lf %lf %lf) Att (pry): (%lf %lf %lf)\n",
				count_messages, td.x, td.y, td.z, td.pitch, td.roll, td.yaw);
		sleep(1);
	}
}
