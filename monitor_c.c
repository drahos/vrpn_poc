#include <stdio.h>
#include <unistd.h>

#include "vrpn_tracker.hpp"

static int count_messages = 0;
static void cb(struct ucart_vrpn_TrackerData * td)
{
	count_messages++;
}

int main(void)
{
	struct ucart_vrpn_tracker * t = NULL;
      t = ucart_vrpn_tracker_createInstance("UAV@192.168.0.120:3883");
	ucart_vrpn_tracker_addCallback(t, cb);
	for(int i = 0; i < 15; i++) {
		struct ucart_vrpn_TrackerData td;
		ucart_vrpn_tracker_getData(t, &td);
         	printf("(%d) FPS: %lf Pos (xyz): (%lf %lf %lf) Att (pry): (%lf %lf %lf)\n",
                	count_messages, td.fps, td.x, td.y, td.z, td.pitch, td.roll, td.yaw);
        	sleep(1);
	}
	ucart_vrpn_tracker_freeInstance(t);

	return 0;
}
