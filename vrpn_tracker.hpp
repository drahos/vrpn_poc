/* Author: Jake Drahos
 *
 * VPRN tracker module header file.
 */

#ifndef _MICROCART_VRPN_TRACKER_HPP
#define _MICROCART_VRPN_TRACKER_HPP

#ifdef __cplusplus
#include <mutex>
#include <thread>
#include <functional>
#include <vector>

#include "vrpn_Tracker.h"
#endif

#include <sys/time.h>

#ifdef __cplusplus
extern "C" 
{
#endif
	struct ucart_vrpn_tracker;
	struct ucart_vrpn_TrackerData {
		double x;
		double y;
		double z;

		double pitch;
		double roll;
		double yaw;

		double fps;
		struct timeval timestamp;
	};

	struct ucart_vrpn_tracker * ucart_vrpn_tracker_createInstance(
			const char * server);

	void ucart_vrpn_tracker_freeInstance(struct ucart_vrpn_tracker * inst);

	int ucart_vrpn_tracker_addCallback(struct ucart_vrpn_tracker * inst,
			void (*cb)(struct ucart_vrpn_TrackerData *));

	int ucart_vrpn_tracker_getData(struct ucart_vrpn_tracker * inst,
			struct ucart_vrpn_TrackerData * td);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
namespace microcart
{
	struct TrackerData {
		double x;
		double y;
		double z;

		double pitch;
		double roll;
		double yaw;

		double fps;
		timeval timestamp;
	};

	class Tracker {
	public:
		const struct TrackerData getData(void);
		void callback(const vrpn_TRACKERCB t);

		Tracker(std::string server);
		Tracker(const char * server);
		~Tracker();

		void addCallback(std::function<void(const TrackerData &)> cb);
	private:
		int stop_flag;
		TrackerData trackerData;
		std::thread vrpn_thread;
		std::mutex vrpn_mutex;

		vrpn_Tracker_Remote remote;

		void vrpn_loop(void);

		std::vector<std::function<void(const TrackerData &)>> cb_vector;
	};


}
/* __cplusplus */
#endif

#endif
